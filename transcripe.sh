#!/bin/bash
#PBS -l ngpus=2

# INPUT
path_to_audio_file=./audio/Allgemeinmedizin.m4a
path_to_venv=../envs/whisper/bin/activate

# nothing to do beloew this line

cd $PBS_O_WORKDIR

echo "starting..."
echo "processing file $path_to_audio_file ..."

# make dir to save transcription and log file
f=$(basename "$path_to_audio_file" | cut -d. -f1)
path_to_transcript_dir=transcripts/"$f"
mkdir -p $path_to_transcript_dir

# measure duration of audio file using ffmpeg and save to log
echo "Duration of input audio file:" > $path_to_transcript_dir/log.txt
ffmpeg -i "$path_to_audio_file" 2>&1 | grep Duration >> $path_to_transcript_dir/log.txt

# run python script for transcription
source $path_to_venv
python transcripe.py "$path_to_audio_file" $path_to_transcript_dir/transcript.txt $path_to_transcript_dir/log.txt
deactivate

echo "done"
