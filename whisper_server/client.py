import requests

# Set the URL for the Flask server
url = 'http://localhost:5000/predict'

# Set the file path for the audio file to be predicted
audio_file_path = '../whisper_test/audio/Allgemeinmedizin.m4a'

# Send a POST request to the Flask server with the file path as data
response = requests.post(url, data={'audio_file_path': audio_file_path})

# Print the predicted output
print(response.text)
