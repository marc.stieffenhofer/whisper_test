from flask import Flask, request
import torch
import whisper
from transformers import pipeline
import os
from timeit import default_timer as timer

app = Flask(__name__)

# set torch device
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Found a GPU!")
else:
    device = torch.device("cpu")
    print("Couldn't find GPU, will use CPU instead!")


# select pretrained model and create pipeline
start = timer()
pipe = pipeline(model="openai/whisper-large", device=device)
print("Successfully loaded model. This took {:.2f} secs \n".format(timer() - start))

@app.route('/predict', methods=['POST'])
def predict():

    start = timer()

    # Get the input data from the request
    audio_file_path = request.form.get('audio_file_path')

    # transcripe
    audio = whisper.load_audio(audio_file_path)
    predictions = pipe(audio, generate_kwargs={"language": "<|de|>", "task": "transcribe"}, chunk_length_s=20,
                       stride_length_s=(5))
    transcript = predictions["text"]

    print("Transcription took {:.2f} secs \n".format(timer() - start))

    return transcript

if __name__ == '__main__':
    # Run the Flask application
    app.run(debug=True,  use_reloader=False)
