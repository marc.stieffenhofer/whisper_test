#!/bin/bash
#PBS -l ngpus=2

cd $PBS_O_WORKDIR

echo "starting..."

source ./BUILD/venv/bin/activate
python main.py 
deactivate

echo "done"
