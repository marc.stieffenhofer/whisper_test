import torch
import whisper
from transformers import pipeline
from timeit import default_timer as timer
import argparse

# get audio file and output paths from command line argument
parser = argparse.ArgumentParser()
parser.add_argument("audio")
parser.add_argument("output")
parser.add_argument("log")
args = parser.parse_args()
audio_file = args.audio
output = args.output
log = args.log

# set torch device
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("GPU will be used!")
else:
    device = torch.device("cpu")
    print("Couldn't find GPU, will use CPU instead!")

# select pretrained model and create pipeline
start = timer()
pipe = pipeline(model="openai/whisper-large", device=device)
with open(log, 'a') as f:
    f.write("Wallclock time to establish pipeline: ")
    f.write("{:.2f} secs \n".format(timer() - start))

# predict transcription
start2 = timer()
audio = whisper.load_audio(audio_file)
predictions = pipe(audio, generate_kwargs={"language":"<|de|>", "task": "transcribe"}, chunk_length_s=20, stride_length_s=(5))
with open(log, 'a') as f:
    f.write("Wallclock time to run prediction: ")
    f.write("{:.2f} secs \n".format(timer() - start2))
    f.write("Total Wallclock time: ")
    f.write("{:.2f} secs \n".format(timer() - start))

# save to .txt file
predictions = predictions["text"]
with open(output, 'w') as f:
    f.write(predictions)
