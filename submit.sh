#!/bin/bash

. /etc/profile.d/pbs.sh

queue=high-q

echo "<$queue> outer script starting..."
qsub -o log.out -e log.err -Wblock=true -N block -q $queue ./transcripe.sh 
echo "<$queue> outer script done..."
